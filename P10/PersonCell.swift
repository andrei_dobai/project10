//
//  PersonCell.swift
//  P10
//
//  Created by Andrei Dobai on 1/16/17.
//  Copyright © 2017 Andrei Dobai. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
