//
//  ViewController.swift
//  P10
//
//  Created by Andrei Dobai on 1/16/17.
//  Copyright © 2017 Andrei Dobai. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    var people=[Person]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self,                        action: #selector(addNewPerson))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self,                        action: #selector(removeStoredItems))
        let defaults = UserDefaults.standard
        if let savedPeople = defaults.object(forKey: "people") as? Data
        {
            people = NSKeyedUnarchiver.unarchiveObject(with: savedPeople) as! [Person]
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        let temp = people[sourceIndexPath.row]
        people[sourceIndexPath.row] = people[destinationIndexPath.row]
        people[destinationIndexPath.row] = temp
        save()
    }
    
    func removeStoredItems()
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "people")
        save()
        collectionView?.reloadData()
    }
    func addNewPerson()
    {
        let picker=UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {return}
        let imageName=UUID().uuidString
        let imagePath=getDocumentsDirectory().appendingPathComponent(imageName)
        if let jpegData=UIImageJPEGRepresentation(image, 80)
        {
            try? jpegData.write(to: imagePath)
        }
        let person=Person(name: "Unknown", image: imageName)
        people.append(person)
        collectionView?.reloadData()
        dismiss(animated: true)
        save()
    }
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return people.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Person", for: indexPath) as! PersonCell
        
        let person = people[indexPath.item]
        
        cell.name.text = person.name
        
        let path = getDocumentsDirectory().appendingPathComponent(person.image)
        let personImage = UIImage(contentsOfFile: path.path)
        cell.imageView.image = personImage
        cell.imageView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.imageView.layer.borderWidth = 2
        cell.imageView.layer.cornerRadius = 3
        cell.layer.cornerRadius = 7
        
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let ac=UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Rename", style: .default){ [unowned self] _ in
            self.renamePersonAlert(indexPath: indexPath)
        })
        ac.addAction(UIAlertAction(title: "Delete", style: .default){ [unowned self] _ in
            self.deletePersonAlert(indexPath: indexPath, collectionView: collectionView)
        })
        ac.addAction(UIAlertAction(title: "View Fullscreen", style: .default){ [unowned self] _ in
            self.showImage(at: indexPath)
        })
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(ac, animated: true)
    }
    func showImage(at indexPath: IndexPath)
    {
        let path = getDocumentsDirectory().appendingPathComponent(people[indexPath.item].image)
        let personImage = UIImage(contentsOfFile: path.path)
        self.performSegue(withIdentifier: "fullScreenImageSegue", sender: personImage)
    }
    func save()
    {
        let savedData = NSKeyedArchiver.archivedData(withRootObject: people)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: "people")
    }
    func renamePersonAlert(indexPath: IndexPath)
    {
        let person=people[indexPath.item]
        let ac=UIAlertController(title: "Rename person", message: nil, preferredStyle: .alert)
        ac.addTextField()
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        ac.addAction(UIAlertAction(title: "Ok", style: .default){ [unowned self, ac] _ in
            let newName=ac.textFields![0]
            person.name=newName.text!
            self.collectionView?.reloadData()
            self.save()
        })
        present(ac, animated: true)
    }
    func deletePersonAlert(indexPath: IndexPath, collectionView: UICollectionView)
    {
        let person = people[indexPath.item]
        let ac=UIAlertController(title: "Are you sure you want to delete \(person.name)?", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [unowned self] _ in
            self.people.remove(at: indexPath.item)
            self.save()
            collectionView.reloadData()
        }))
        present(ac, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "fullScreenImageSegue"
        {
            let vc = segue.destination as! FullScreenImage
            let personImage = sender as! UIImage
            vc.selectedImage = personImage
        }
    }
}

