//
//  FullScreenImage.swift
//  P10
//
//  Created by Andrei Dobai on 1/19/17.
//  Copyright © 2017 Andrei Dobai. All rights reserved.
//

import UIKit

class FullScreenImage: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    var selectedImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = selectedImage
        {
            imageView.image = image
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
