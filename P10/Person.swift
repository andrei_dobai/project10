//
//  Person.swift
//  P10
//
//  Created by Andrei Dobai on 1/18/17.
//  Copyright © 2017 Andrei Dobai. All rights reserved.
//

import UIKit

class Person: NSObject, NSCoding
{
    required init(coder aDecoder: NSCoder)
    {
        name=aDecoder.decodeObject(forKey: "name") as! String
        image=aDecoder.decodeObject(forKey: "image") as! String
    }
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(image, forKey: "image")
    }
    var name: String
    var image: String
    init(name: String, image: String)
    {
        self.image=image
        self.name=name
    }
}
